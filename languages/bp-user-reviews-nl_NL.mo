��    4      �  G   \      x  
   y     �     �     �     �     �     �     �     �     �     �       $   &  /   K     {     �     �     �     �     �     �     �     �          
               -  %   C     i     q     ~     �     �     �     �     �     �  	   �     �     �     �     �      �       
   2  	   =     G     X  R   l     �  �  �     q	     �	  !   �	     �	     �	  ,   �	     �	     
     
     #
     0
     M
  *   i
  ,   �
     �
  
   �
  (   �
     
           %     F     a     e  
   |     �     �     �  !   �  /   �               .     D     Q     X     ]     e     s     {  	   �     �  '   �     �     �     �            	   '     1  I   6     �     '          0          1                                
       *   !          /   (      3       &   .         4   	      2   $                   -   #   %             )       +                                          ,             "                   Add Review Add new Already reviewed by you Anyone Author Auto approve new reviews? BP User Reviews Settings Criteria Date Date Format Email field is required Email is wrong How many stars is the rating out of? Is a text review required alongside the rating? Logged in users Marks Min. Review Length Multiple Multiple criteria Multiple reviews allowed? Name field is required No No reviews yet Publish Rating scale Review Review Details Review can`t be empty Review must be at least %s characters Reviews Save Changes Saved successfully. Settings Single Spam Stars Stars color Total: Unpublish User User Reviews Who can leave reviews? Yes You can not put yourself reviews You must select all stars Your email Your name commentNot Spam comment statusSpam commentsSpam <span class="count">(%s)</span> Spam <span class="count">(%s)</span> of Project-Id-Version: BP Member Reviews 1.1.4
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/bp-user-reviews
POT-Creation-Date: 2016-11-22 11:44+0100
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-11-23 10:28+0100
Language-Team: Monkidoo
X-Generator: Poedit 1.8.11
Last-Translator: Koen Schaper
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nl_NL
 Beoordeling toevoegen Nieuwe toevoegen Je hebt hem of haar al beoordeeld Iedereen Auteur Nieuwe beoordelingen automatisch goedkeuren? BP User Reviews instellingen Criteria Datum Datumnotatie Het e-mail veld is verplicht Het e-mailadres is verkeerd Uit hoeveel sterren bestaat de waardering? Is een verklaring nodig naast de waardering? Ingelogde gebruikers Waardering Minimale lengte van de beoordelingstekst Meerdere Meerdere criteria Meerdere beoordelingen mogelijk? Het naam veld is verplicht Nee Nog geen beoordelingen Publiceren Waarderingsschaal Beoordeling Beoordelingsgegevens De beoordeling kan niet leeg zijn De beoordeling moet tenminste %s karakters zijn Beoordelingen Wijzigingen opslaan Met succes opgeslagen Instellingen Enkele Spam Sterren Sterren kleur Totaal: Wachtend op review Gebruiker Beoordelingen Wie kan er een beoordeling achterlaten? Ja Je kunt niet jezelf beoordelen Je moet alle sterren selecteren Je e-mailadres Je naam Geen Spam Spam Spam <span class="count">(%s)</span> Spam <span class="count">(%s)</span> van 